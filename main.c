//SprinklerController main.c code


#include "main.h"

typedef enum
{
  All_Stop_State,
  First_Warning_State,
  Second_Warning_State,
  Get_Temp_State,
}eSystemState;

/* Prototype Event Handlers */
eSystemState Passive_State(void)
{

    HAL_GPIO_WritePin(GPIOB, Yellow_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, Red_LED_Pin, GPIO_PIN_RESET);

	return Get_Temp_State;
}

eSystemState Yellow_Warning_State(void)
{
    HAL_GPIO_WritePin(GPIOB, Yellow_LED_Pin, GPIO_PIN_SET);

	return Get_Temp_State;
}

eSystemState Red_Alert_State(void)
{

   HAL_GPIO_WritePin(GPIOX, Red_LED_Pin, GPIO_PIN_SET);
   
    return Get_Temp_State;
}

eSystemState getting_Temp_State(void)
{
	HAL_ADC_Start(&hadc1);
		HAL_ADC_PollForConversion(&hadc1,5)
		{
			adcval = HAL_ADC_GetValue(&hadc1);
			Temp = ( Adcval * 330 ) / 1023;   /// for 10 Bit resolution  
		}
		
		    if (Temp < 32 )
			{
				return All_Stop_State; 
			}
			else if ((Temp >= 32) && (Temp < 40))
			{
				return First_Warning_State;
			}
			else (Temp > 40)
			{
				 return Second_Warning_State ;
			}
}


uint16_t adcval; // type definition of raw value. 
float Temp ;

int main(void)
{
	
	/* Declare eNextState and initialize it to All Stope */ 
	eSystemState eNextState = All_Stop_State ;
	
	/* Infinite loop */
  /* USER CODE BEGIN WHILE */
	While (1)
	{
		
	
	   
	   switch(eNextState)
	      {
		  case All_Stop_State : 
		    {
				eNextState = Passive_State();
			}
			break ;
		  case First_Warning_State : 
		    {
				eNextState = Yellow_Warning_State();
			}

		    break;
		  case Second_Warning_State :
			{
				eNextState = Red_Alert_State();
			}
			break ;
			
		  case Get_Temp_State :
		    {
				eNextState = getting_Temp_State();
			}
			
	}
	
} /* USER CODE END 3 */